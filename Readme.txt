To use the makefile:
	Type 'nmake load' and press enter to compile the program.
	Type 'nmake clean' to delete the *.obj and *.exe files.
To test the program, type 'test' (after compiling) and press enter.
The toolchain used is the Microsoft compiler.
