#include "binary_tree.h"
#include <vector>
#include <iostream>

using namespace std;

// Creates an empty binary tree 
binary_tree::binary_tree()
{

}

// Creates a binary tree with an initial value to store
binary_tree::binary_tree(int value)
{
	tree = new node;
	//Dereferencing tree to get initial node's data, setting it to value.
	(*tree).data = value;
}

// Creates a binary tree from a collection of existing values
binary_tree::binary_tree(const std::vector<int> &values)
{
	for(int i = 0; i < values.size(); i++)
	{
		insert(values[i]);
	}
}

//Binary tree copy helper
node* myNewTree (node *rhs)
{
	node* newtree = nullptr; 
	if(rhs != nullptr)
	{
		newtree = new node;
		newtree->left = myNewTree(rhs->left);
		newtree->data = rhs->data;
		newtree->right = myNewTree(rhs->right);				
	}
	return newtree;
}

// Creates a binary tree by copying an existing tree
binary_tree::binary_tree(const binary_tree &rhs)
{
	tree = myNewTree(rhs.tree);
}

void delete_tree(node *tree)
{
	if (tree != nullptr)
	{
		delete_tree(tree->left);
		delete_tree(tree->right);
		delete tree;
	}
}


bool equalTreeTest(node *tree, node* tree2)
{
	if (tree != nullptr)
	{
		if(tree->data == tree2->data)
		{
			equalTreeTest(tree->left, tree2->left);
			equalTreeTest(tree->right, tree2->right);
		}
		else
		{
			return false;
		}
	}
	return true;
}

// Destroys (cleans up) the tree
binary_tree::~binary_tree()
{
	delete_tree(tree);
}

void insertShell (node** currenttree, int value)
{
	//Check if nullptr. If so set new node
	if (*currenttree == nullptr)
	{
		//Create new node
		*currenttree = new node;
		//Set new value
		(**currenttree).data = value;
		//Set branches to nullptr
		(**currenttree).left = nullptr;
		(**currenttree).right = nullptr;
	}
	else
	{
		if (value < (**currenttree).data)
		{
			insertShell(&(**currenttree).left,value);
		}
		else if (value > (**currenttree).data)
		{
			insertShell(&(**currenttree).right,value);
		}
		else
		{
			return;
		}
	}
}

// Adds a value to the tree
void binary_tree::insert(int value)
{
	node** pointer = &tree;
	insertShell(pointer, value);
}

void removeHelper (node *tree, int value)
{
	if(tree == nullptr)
	{
		cout << "This does not exist";
	}
	
	if((tree->data == value) && (tree->left == nullptr) && (tree->right == nullptr))
	{
		delete tree;
	}
	else if((tree->data == value) && (tree->left != nullptr) && (tree->right == nullptr))
	{
		
	}
	else if((tree->data == value) && (tree->left == nullptr) && (tree->right != nullptr))
	{
		
	}
	else if((tree->data == value) && (tree->left != nullptr) && (tree->right != nullptr))
	{
		
	}
	else
	{
		removeHelper(tree->left, value);
		removeHelper(tree->right, value);
	}
}

// Removes a value from the tree
void binary_tree::remove(int value)
{
	/*
	removeHelper(tree, value);
	*/
}

bool search(node *tree, int term)
{
	if(tree == nullptr)
	{
		return false;
	}
	if(tree->data == term)
	{
		return true;
	}
	else
	{
		search(tree->left, term);
		search(tree->right, term);
	}
}

// Checks if a value is in the tree
bool binary_tree::exists(int value) const
{
    return (search(tree, value));
}

//Sorts in order
std::string inorderShell(node *tree)
{
	std::string output = std::string("");
	if(tree != nullptr)
	{
		output += inorderShell(tree->left);
		output += std::to_string(tree->data) + " ";
		output += inorderShell(tree->right);			
	}
	return output;
}

// Prints the tree to standard out in numerical order
std::string binary_tree::inorder() const
{
	std::string tempout = inorderShell(tree);
	if(tempout.length() > 1)
		tempout.pop_back();
    return tempout;
}

//Sorts in preorder
std::string preorderShell(node *tree)
{
	std::string output = std::string("");
	if(tree != nullptr)
	{
		output += std::to_string(tree->data) + " ";
		output += preorderShell (tree->left);
		output += preorderShell (tree->right);
	}
	return output;
}

// Prints the tree to standard out in preorder
std::string binary_tree::preorder() const
{
    std::string tempout = preorderShell(tree);
	if(tempout.length() > 1)
		tempout.pop_back();
    return tempout;
}

//Sorts in postorder
std::string postorderShell(node *tree)
{
	std::string output = std::string("");
	if(tree != nullptr)
	{
		output += postorderShell (tree->left);
		output += postorderShell (tree->right);
		output += std::to_string(tree->data) + " ";
	}
	return output;
}

// Prints the tree to standard out in postorder
std::string binary_tree::postorder() const
{
    std::string tempout = postorderShell(tree);
	if(tempout.length() > 1)
		tempout.pop_back();
    return tempout;
}

//NEEDS COMPLETED---------------------------------------------------

// Copy assignment operator
binary_tree& binary_tree::operator=(const binary_tree &other)
{
	node *tree2 = nullptr;
    tree2 = myNewTree(other.tree);
	return *this;
}

// Checks if two trees are equal
bool binary_tree::operator==(const binary_tree &other) const
{
    return (equalTreeTest(tree, other.tree));
}

// Checks if two trees are not equal
bool binary_tree::operator!=(const binary_tree &other) const
{
    return true;
}

// Inserts a new value into the binary tree
binary_tree& binary_tree::operator+(int value)
{
	insert(value);
	return *this;
}

// Removes a value from the binary tree
binary_tree& binary_tree::operator-(int value)
{
    return *this;
}

// Reads in values from an input stream into the tree
std::istream& operator>>(std::istream &in, binary_tree &value)
{
    return in;
}

// Writes the values, in-order, to an output stream
std::ostream& operator<<(std::ostream &out, const binary_tree &value)
{
    return out;
}